package com.coa.qa.properties;

public interface SalesforceProperties {

	public static String quickBaseURL = "https://clearonedebt.quickbase.com/db/main?a=SignIn";
	public static String loginEmailSelectorType = "name";
	public static String loginEmailSelector = "loginid";
	public static String loginEmailValue = "dhossain@clearoneadvantage.com";
	
	public static String passwordSelectorType ="name";
	public static String passwordSelector ="password";
	public static String passwordvalue ="01922Dipu";
	
	public static String signInSelectorType= "id";
	public static String signInSelector= "signin";
	
	public static String clearOneDebtSelectorType ="xpath";
	public static String clearOneDebtSelector ="//body/b[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[3]/div[2]/div[2]/div[3]/a[1]/div[1]";
	
	public static String clientSelectorType ="id";
	public static String clientSelector ="tbl_bdp9uwef7";
	
	public static String clientRecordIdSelectorType ="id";
	public static String clientRecordIdSelector ="matchText_0";
	public static String clientRecordId ="153844";
//	153844(original recorID)
	
	public static String searchBtnSelectorType ="id";
	public static String searchBtnSelector ="tableHomePageSearch";
	
	public static String clientProfileSelectorType ="css";
	public static String clientProfileSelector ="tr.od";
	
	public static String calculatorNewSelectorType ="xpath";
	public static String calculatorNewSelector ="//a[contains(text(),'Calculator (New)')]";
	
	public static String advantagepointUserNameSelectorType ="id";
	public static String advantagepointUserNameSelector ="ctl00_ContentPlaceHolder1_Login1_UserName";
	public static String advantagepointUserName ="dhossain";
	
	public static String advantagepointPasswordSelectorType ="id";
	public static String advantagepointPasswordSelector ="ctl00_ContentPlaceHolder1_Login1_Password";
	public static String advantagepointPassword ="IDbREZF^tL+Omm";
	
	public static String advantagepointLogInSelectorType ="id";
	public static String advantagepointLogInSelector ="ctl00_ContentPlaceHolder1_Login1_LoginButton";
	
	public static String negotitionCalculatorSelectorType ="id";
	public static String negotitionCalculatorSelector ="ctl00_ContentPlaceHolder1__HyperLink_To_Calculator_2";
	
	
	public static String recordId ="1181611";
//	1181611(original recordID)
//	public static String negoCalcUrl = "https://clearoneadvantage.lightning.force.com";
//	public static String negoCalcUrl ="https://iweb.clearoneadvantage.com/Internal_website/Internal/ClearOne/Calculator.aspx?Topbar=0&RecordID="+recordId+"&Cal_ID=11651520";
//  1190179(alter recordID)
	public static String optionOneSelectorType ="xpath";
	public static String optionOneSelector ="//a[@id='ctl00_ContentPlaceHolder1__Creditor_Settlement_Automation__FormView_Settlement_Options_ctl00_BTN_Option']";
	
	public static String optionsSelectorType="css";
	public static String optionsSelector="td.col-sm-5ths";
	
	public static String settelementSelectorType ="class";
	public static String settelementSelector ="pl-2";
	
	public static String settlementFieldSelectorType = "name";
	public static String settlementFieldSelector ="ctl00$ContentPlaceHolder1$_Creditor_Settlement_Automation$_FormView_Settlement_Info$Label_Settlement";
	
	public static String currentBalanceselectorType="xpath"; 
	public static String currentBalanceselector="//span[@id='currentBalanceText']";
	
	
	
	public static String credirInfoSelectortype="css";
	public static String creditorInfoSelector="table.Gridview_Table";
	
	public static String selectorType ="css";
	public static String selector ="span[class=mx-auto]";
	
	public static String optionTwoSelectorType = "id";
	public static String optionTwoSelector ="ctl00_ContentPlaceHolder1__Creditor_Settlement_Automation__FormView_Settlement_Options_ctl01_BTN_Option";
	
	public static String optionThreeSelectorType = "xpath";
	public static String optionThreeSelector = "//span[contains(text(),'Select Option #3')]";
	
	public static String optionFourSelectorType = "xpath";
	public static String optionFourSelector = "//span[contains(text(),'Select Option #4')]";
	
	public static String settlementSelectorType = "css";
	public static String settlementSelector = "td.col-sm-9";
	
	public static String addNewPaymentSelectorType ="xpath";
	public static String addNewPaymentSelector = "//input[@id='ctl00_ContentPlaceHolder1__Creditor_Settlement_Automation_Button3']";
	
	public static String addPaymentSelectorType = "css";
	public static String addPaymentSelector = ".table-responsive.Gridview_Table td:nth-of-type(5) > [value='$0.00']";
	public static String addPaymentSelectorNew = ".table-responsive.Gridview_Table tr:nth-of-type(3) > td:nth-of-type(5) > .mx-auto";
	public static String paymentAmount = "$30.00";

	public static String addAnotherPaymentSelectorType = "xpath";
	public static String addAnotherPaymentSelector = "//input[@id='ctl00_ContentPlaceHolder1__Creditor_Settlement_Automation_PaymentSummaryGrid_ctl04_Label_Amount']";
//			".table-responsive.Gridview_Table tr:nth-of-type(5) > td:nth-of-type(5) > .mx-auto";
	public static String anotherPaymentAmount = "$20.00";
	
	
	public static String modifyPaymentSelectorType = "xpath";
	public static String modifyPymentSelector = "//input[@id='ctl00_ContentPlaceHolder1__Creditor_Settlement_Automation_PaymentSummaryGrid_ctl02_Label_Amount']";
	public static String modifyPayment ="$1,700.00";
	
	public static String salesforceUrl = "https://clearoneadvantage--stage.my.salesforce.com/";
	
	
	
	public static String usernameSelectorType = "xpath";
	public static String usernameSelector = "//input[@id='username']";
	public static String sfUsername = "dhossain@clearoneadvantage.com.stage";
	
	
	public static String passSelectorType = "xpath";
	public static String passSelector ="//input[@id='password']";
	public static String sfPassword = "Dipu@Pretty25"; 
	
	
	public static String sandboxSelectorType = "xpath";
	public static String sandboxSelector = "//input[@id='Login']";
	
	
	public static String nevigationMenuSelectorType ="xpath";
	public static String nevigationMenuSelector = "//button[@title='Show Navigation Menu']//lightning-primitive-icon";
	
	public static String menuOptionSelectorType = "xpath";
	public static String menuOptionSelector = "//div[@class='slds-media slds-listbox__option slds-listbox__option_entity slds-p-around_none']";
	
	public static String newButtonSelectorType ="xpath";
	public static String newButtonSelector = "//div[@title='New']";
	
	public static String firstNameSelectorType ="xpath";
	public static String firstNameSelector = "//input[@name='firstName']";
	public static String firstNameText = "Oscar";
	
	public static String lastNameSelectorType = "xpath";
	public static String lastNameSelector = "//input[@name='lastName']";
	public static String lastNameText ="Winner";
	
	public static String emailSelectorType = "xpath";
	public static String emailSelector = "//input[@name='Email']";
	public static String emailText = "test.dhossain@gmail.com";
	
	public static String saveButtonSelectorType = "xpath";
	public static String saveButtonSelector = "//button[@class='slds-button slds-button_brand']";
	
	public static String ssnSelectorType = "xpath";
	public static String ssnSelector = "//input[@name='Social_Security_Number__c']";
	public static String ssnText = "000-00-0000";
	
	public static String dobSelectorType = "xpath";
	public static String dobSelector = "//input[@name='Date_of_Birth__c']";
	public static String dobText = "07/01/1990";
	
	public static String nextSelectorType = "xpath";
	public static String nextSelector = "//span[contains(text(),'Next')]";
	
	public static String draftfrequencyListSelectorType = "xpath";
	public static String draftfrequencyListSelector = "//lightning-picklist//lightning-combobox";
	public static String draftfrequencySelector = "//lightning-base-combobox//input";
	
	public static String draftFrequencyMonthlySelectorType = "xpath";
	public static String draftFrequencyMonthlySelector = "//lightning-base-combobox-item[@data-value='Monthly']";
	
	
	public static String programStartDateSelectorType = "xpath";
	public static String programStartDateSelector = "//lightning-datepicker//input[@name='Program_Start_Date__c']";
	public static String programStartDateText = "11/30/21";
	
	public static String firstSplitDraftAmountSelectorType = "xpath";
	public static String firstSplitDraftAmountSelector = "//input[@name='First_Split_Draft_Amount__c']";
	public static String firstSplitDraftAmount = "50";
	
	public static String secondSplitDraftAmountSelectorType = "xpath";
	public static String secondSplitDraftAmountSelector = "//input[@name='Second_Split_Draft_Amount__c']";
	public static String secondSplitDraftAmount = "75";
	
	public static String draftNoteListSelectorType = "xpath";
	public static String draftNoteListSelector = "//lightning-textarea//textarea";
	
	public static String closeDateListSelectorType = "xpath";
	public static String closeDateListSelector = "//lightning-input//lightning-datepicker";
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
