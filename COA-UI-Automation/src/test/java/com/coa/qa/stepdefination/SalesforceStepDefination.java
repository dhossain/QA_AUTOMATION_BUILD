package com.coa.qa.stepdefination;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.coa.qa.framework.database.DatabaseConnection;
import com.coa.qa.framework.helper.BaseTest;
import com.coa.qa.pageObj.SalesforceUAT;
import com.coa.qa.properties.SalesforceProperties;
import com.coa.qa.properties.Props;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SalesforceStepDefination extends BaseTest {
	
	SalesforceUAT sfdc = new SalesforceUAT();
	
	DatabaseConnection dbConn = new DatabaseConnection();
	
  @Given("^User nevigate to QuickBase SignIn Page")
  public void User_nevigate_to_QuickBase_SignIn_Page() throws Throwable {
	  
	  navigationObj.navigateTo(SalesforceProperties.quickBaseURL);
	  
	  
  }

  @And("^User Provide Email Or Username")
  public void User_Provide_Email_Or_Username() throws Throwable {
	  
	  sfdc.enterEmail();
	  
	  
  }

  @And("^User provide Password")
  public void User_provide_Password() throws Throwable {
	  
	  sfdc.enterPassword();
	  
	  
	  
  }

  @Then("^User Click On SignIn button")
  public void User_Click_On_SignIn_button() throws Throwable {
	  
	  
	  sfdc.signInClick();
	  
	  
  }
  
  @Given("^User Click On Clear One Debt button")
  public void User_Click_On_Clear_One_Debt_Button() throws Throwable{
	  
	  sfdc.clearOnedebt();
	  
  }

 @And("^User Nevigate To Client")
 public void User_Nevigate_To_Client() throws Throwable{
	 
	 sfdc.nevigateClient();
	 
 }
  
 @And("^User Search For Client")
 public void User_Search_For_Client() throws Throwable{
	 
	 sfdc.searchClient();
	 
	 
 }
 
 @And("^User Click On Client Record")
 public void User_Click_On_Client_Record() throws Throwable{
	 
	 sfdc.clientrecord();
	 
	 
 }
 
 @Then("^User Click On New Calculator")
 public void User_Click_On_New_Calculator() throws Throwable{
	 
	 
	 sfdc.newCalculator();
	 
	 
 }
 @When("^User Move To Advantage point Window")
 public void User_Move_To_Advantage_point_Window() throws Throwable {
	 
	 sfdc.moveToAdvantagePointWindow();
 }
 
 @Given("^User Enter Credential")
 public void User_Enter_Credential() throws Throwable{
	 
	 
	 sfdc.advantagePointLogIN();
	  
 }

 @Then("^User Click on Log In Now Btn")
 public void User_Click_on_Log_In_Now_Btn() throws Throwable{
	 
	 sfdc.logInNow();
	  
 }
 @Given("^User Click On To Calculator 3")
 public void User_Click_On_To_Calculator_3() throws Throwable{
	 
	 sfdc.clickNegotitionCalculator();
	 
 }
 @And("^User Move To Negotition Calculator window")
 public void User_Move_To_Negotition_Calculator_window() throws Throwable{
	 
	 sfdc.moveToNegotitionCalc();
	 
 }
 @Given("^User Provide appropriate Calculator URL")
 public void User_Provide_appropriate_Calculator_URL() throws Throwable{
	 
	 
	 sfdc.directOpenNegotitionCalc();
	 
 }
 @And("^LogIn To Advantage Point")
 public void LogIn_To_Advantage_Point() throws Throwable{
	 
	 sfdc.advantageLogIn();
 }
 
 @Given("^User Click On Select Option 1")
 public void User_Click_On_Select_Option_1() throws Throwable{
	 
	 sfdc.selectOptionOne();
	 
 }
 
 
 @Then("^Validate Debt Amount for \"([^\"]*)\"$")
 public void validate_Debt_Amount_for(String selectType) throws Throwable {
	 
//	 calculator.optionsDebtAmontValidate(selectType);
	 
	 sfdc.optionTwo(selectType);

 }
 
 
 @Given("^Validate Settlement Amount With \\\"([^\\\"]*)\\\"$")
 public void Validate_Settlement_Amount_With (String anyOption) throws Throwable{
	 
	 
//	 calculator.optionTwo(selectType);
	 
	 dbConn.displayDbProperties();
	 
	 
//	 calculator.getSelectOptionsElements(anyOption);
	 
	 
 }

 @When("^User click on select option one")
 public void User_click_on_select_option_one()throws Throwable{
	 
	 
	 sfdc.selectOptionOne();
	 
	 
 }
 
 @And("^Get settlement amount from payments field")
 public void Get_settlement_amount_from_payments_field()throws Throwable{
	 
	 sfdc.getTotallsettlementamount();
	  
 }
 
 @And("^Get Settlement Amount from Select option One")
 public void Get_Settlement_Amount_from_Select_option_One()throws Throwable{
	 
	 sfdc.selectOptionOneElement();
 }
 
 @Then("^Validate settlement Amount With Payment total Amount")
public void  Validate_settlement_Amount_With_Payment_total_Amount()throws Throwable{
	 
	 sfdc.validateSettlementAmountWithPaymenttotal();
 }
 
 
 @When("^User Click On Select Option Two$")
 public void user_Click_On_Select_Option_Two() throws Throwable {
     
	 sfdc.clickOptionTwo();
 
 }
 
 @When("^Get Settlement Amount Totall From Payments Table$")
 public void get_Settlement_Amount_Totall_From_Payments_Table() throws Throwable {
     
	 sfdc.totalPayments();
	 
 }
 
 @When("^Get Settlement Amount From Select Option Two$")
 public void get_Settlement_Amount_From_Select_Option_Two() throws Throwable {
	 
     sfdc.selectOptionTwo();

 }
 
 @Then("^Validate Settlement Amount$")
 public void validate_Settlement_Amount() throws Throwable {
     
	 sfdc.validateOptionTwoSettlementAmount();
	 
	 
 }

 @When("^User Click on Select Option Three")
 public void User_Click_on_Select_Option_Three() throws Throwable{
	 
	 sfdc.clickOptionThree();
	 
 }
 @And("^Get Total Amount From Payments Table")
 public void Get_Total_Amount_From_Payments_Table() throws Throwable{
	 
	 sfdc.getTotalPaymentAmount();
 }
 @And("^Get Settlement Amount From Select option Three")
 public void Get_Settlement_Amount_From_Select_option_Three() throws Throwable{
	 
	 sfdc.getSetlementAmount();
	 
 }
 @And("^Validate Settelement Amount with Total Amount")
 public void Validate_Settelement_Amount_with_Total_Amount() throws Throwable{
	 
	sfdc.validateoptionThreeSettlementAmount();
	 
 }
 @When("^User Click On Option Four")
 public void  User_Click_On_Option_Four() throws Throwable{
	 
	 sfdc.clickOptionFour();
 }
 @When("^Get Total Setlemnt Amount From Payments Table$")
 public void get_Total_Setlemnt_Amount_From_Payments_Table() throws Throwable {
     
	 sfdc.getTotalAmount_OptionFour();
 }
 
 @When("^Get Settlement Amount From Option Four$")
 public void get_Settlement_Amount_From_Option_Four() throws Throwable {
   
	 sfdc.getSetlemntAmountForOptionFour();
	 
 }
 
 @Then("^Validate Settlement Amount For Option Four$")
 public void validate_Settlement_Amount_For_Option_Four() throws Throwable {
   
	 sfdc.validateOptionFourSettlementAmount();
	 
 }
 
 @When("^User Click On Add New Payment Bttn")
 public void User_click_on_Add_new_Payment_Bttn() throws Throwable{
	 
	 sfdc.clickAddNewPaymentButton();
	 
 }
 
 @And("^Add New Payment Amount$")
 public void add_New_Payment_Amount() throws Throwable {
     
	 sfdc.addPaymentAmount();
 }
 
 @And("^Add New Payments Amount")
 public void Add_New_Payments_Amount() throws Throwable{
	 
	 sfdc.addNewPayments();
	 
 }
 
 @And("^Modify existing Payment Amount")
 public void Modify_existing_Payment_Amount() throws Throwable{
	 
	 sfdc.modifyExisitingPayment();
	 
 }
 
 @Given("^User navigate to Salesforce")
public void User_navigate_to_Salesforce() throws Throwable{
	 
	 
	 sfdc.nevigateToSalesforce();
	 
	 
 }

@When("^User login to Salesforce$")
public void user_login_to_Salesforce() throws Throwable {
   
	sfdc.loginToSalesforce();
	
}

@Then("^User Click Login to Sandbox$")
public void User_Click_Login_to_Sandbox() throws Throwable {
    
	sfdc.clickLoginToSandbox();
	
}


@Given("^User Click Nevigation Menu$")
public void User_Click_Nevigation_Menu() throws Throwable {
    
	sfdc.clickNevigationMenu();
  
}

@When("^User Click Leads$")
public void User_Click_Leads() throws Throwable {
    
	sfdc.clickLeads();
  
}
 
@And("^User Click New To create new Lead$")
public void User_Click_New_To_create_new_Lead() throws Throwable {
    
	sfdc.clickNewToCreatNewLead();
  
}

@And("^User Enter New Lead Information$")
public void User_Enter_New_Lead_Information() throws Throwable {
    
	sfdc.enterNewLeadInformation();
  
}
@Then("^User Click Save")
public void User_Click_Save () throws Throwable{
	
	sfdc.clickSave();
	
}
@When("^User Click Opportunities")
public void User_Click_Opportunities () throws Throwable{
	
	sfdc.clickOpportunities();
	
}
@And("^User Click New To create new opportunities")
public void User_Click_New_To_create_new_opportunities () throws Throwable{
	
	
	sfdc.clickNewToCreateNewOpportunities();
	
}
@And("^User Click Next")
public void User_Click_Next () throws Throwable{
	
	sfdc.clickNext();
					
				
}
	
@And("^User Enter New Opportunity Information")
	public void User_Enter_New_Opportunity_Information() throws Throwable{
		
	sfdc.draftFrequency();
	
	
	sfdc.selectDraftFrequency();
	
	
	sfdc.enterProgramStartDate();
	
	
	sfdc.enterFirstSplitDraftAmount();
	
	
	sfdc.enterSecondSplitDraftAmount();
	
	sfdc.enterDraftNote();
	
	sfdc.enterCloseDate();
	
}
 










 
 
 
 ////////////////**************///////////////////
 
 
 
 
 
 
 
 
 @Given("^User run a valid query for \"([^\"]*)\"$")
 public void userRunAValidQueryFor(String optinType) throws Throwable {
    	 String url ="jdbc:jtds:sqlserver://MYPC/MYDB;instance=SQLEXPRESS";
	String dbURL = "jdbc:sqlserver:172.27.235.216:1434/COA-BALT-DBDEV\\COABALTSQL02";
	String username = "SLFG\\dhossain";
        String password = "Hos3454#";
        //Load MS SQL JDBC Driver
        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        //Creating connection to the database
        Connection con = DriverManager.getConnection(dbURL,username,password);
        //Creating statement object
    	Statement st = con.createStatement();
    	String selectquery = "SELECT * FROM dbo.adp";
        //Executing the SQL Query and store the results in ResultSet
    	ResultSet rs = st.executeQuery(selectquery);
    	//While loop to iterate through all data and print results
    	while (rs.next()) {
    		System.out.println(rs.getString("Record Id"));
    	}
        //Closing DB Connection
    	con.close();
	}
 }
