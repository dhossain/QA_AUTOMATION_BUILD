package com.coa.qa.pageObj;


import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.asserts.Assertion;
import com.coa.qa.framework.actions.FailedTestCases;
import com.coa.qa.framework.helper.BaseTest;
import com.coa.qa.properties.SalesforceProperties;
import cucumber.deps.com.thoughtworks.xstream.mapper.SystemAttributeAliasingMapper;



public class SalesforceUAT extends BaseTest implements SalesforceProperties{

	
	
	
	public void enterEmail() throws Throwable{
		waitTimeObj.Thread(3000);
		
		inputObj.enterText(loginEmailSelectorType, loginEmailValue, loginEmailSelector);
		
		
	}
	
	public void enterPassword() throws Throwable{
		waitTimeObj.Thread(3000);
		
		inputObj.enterText(passwordSelectorType, passwordvalue, passwordSelector);
		
	}
	
	public void signInClick() throws Throwable{
		waitTimeObj.Thread(3000);
		
		clickObj.click(signInSelectorType, signInSelector);
		
		
	}
	
	public void clearOnedebt() throws Throwable{
		waitTimeObj.Thread(4000);
		
		clickObj.click(clearOneDebtSelectorType, clearOneDebtSelector);
		
		
	}
	
	public void nevigateClient() throws Throwable{
		waitTimeObj.Thread(3000);
		
		clickObj.click(clientSelectorType, clientSelector);
		
	}
	
	public void searchClient() throws Throwable{
		waitTimeObj.Thread(3000);
		
		inputObj.enterText(clientRecordIdSelectorType, clientRecordId, clientRecordIdSelector);
		
		
		waitTimeObj.Thread(2000);
		
		clickObj.click(searchBtnSelectorType, searchBtnSelector);
	}
	
	public void clientrecord()  throws Throwable{
		
		waitTimeObj.Thread(3000);
		
		clickObj.click(clientProfileSelectorType, clientProfileSelector);
		
		
	}
	
	public void newCalculator() throws Throwable{
		
		waitTimeObj.Thread(3000);
		
		clickObj.click(calculatorNewSelectorType, calculatorNewSelector);
		
		
	}
	public void moveToAdvantagePointWindow() throws Throwable {
		
		waitTimeObj.Thread(3000);
		
		navigationObj.moveToNextWindow();
		
	}
	

	public void advantagePointLogIN() throws Throwable{
		
		
		waitTimeObj.Thread(2000);
		
		inputObj.enterText(advantagepointUserNameSelectorType, advantagepointUserName, advantagepointUserNameSelector);
		
		waitTimeObj.Thread(2000);
		
		inputObj.enterText(advantagepointPasswordSelectorType, advantagepointPassword, advantagepointPasswordSelector);
		
		waitTimeObj.Thread(2000);
		
		clickObj.click(advantagepointLogInSelectorType, advantagepointLogInSelector);
	
	}
	
	public void logInNow() throws Throwable{
		waitTimeObj.Thread(2000);
		
		
		clickObj.click(advantagepointLogInSelectorType, advantagepointLogInSelector);
		
	}
	public void clickNegotitionCalculator() throws Throwable{
		waitTimeObj.Thread(2000);
		
		clickObj.click(negotitionCalculatorSelectorType, negotitionCalculatorSelector);
		
	}
	public void moveToNegotitionCalc() throws Throwable {
		
		waitTimeObj.Thread(2000);
		navigationObj.moveToNextWindow();
		
	}
	public void directOpenNegotitionCalc() throws Throwable{
		
		
//		 navigationObj.navigateTo(ClearOneCalculatorProperties.negoCalcUrl);
		
	}
	public void advantageLogIn() throws Throwable{
		
		waitTimeObj.Thread(2000);
		
		inputObj.enterText(advantagepointUserNameSelectorType, advantagepointUserName, advantagepointUserNameSelector);
		
		waitTimeObj.Thread(2000);
		
		inputObj.enterText(advantagepointPasswordSelectorType, advantagepointPassword, advantagepointPasswordSelector);
		
		waitTimeObj.Thread(2000);
		
		clickObj.click(advantagepointLogInSelectorType, advantagepointLogInSelector);
	}
	
	public void selectOptionOne() throws Throwable{
		
		waitTimeObj.Thread(2000);
		
		clickObj.click(optionOneSelectorType, optionOneSelector);
		
		waitTimeObj.Thread(2000);
	}
	
	public void getSelectOptionsElements(String anyOption)  throws Throwable{
		
		List<WebElement> options = new ArrayList();
		List<WebElement> allFields = new ArrayList();
		
		options = getElementsObj.getElementList(optionsSelectorType, optionsSelector);
		
		WebElement option = getElementsObj.getElementFromListByContainText(options, anyOption);
		
		allFields = option.findElements(By.cssSelector("span.pl-2"));
		System.out.println(allFields.size());
		String fieldOne = allFields.get(0).getText();
		String fieldTwo = allFields.get(1).getText();
		String fieldThree = allFields.get(3).getText();
		String fieldFour = allFields.get(4).getText();
		System.out.println(fieldFour);
	}
	

	
	public void optionsDebtAmontValidate(String selectType) throws Throwable{
		
		
		List<WebElement> selectOptions = new ArrayList();
		List<WebElement> selectOptionOne = new ArrayList();
	
		selectOptions = getElementsObj.getElementList(optionsSelectorType, optionsSelector);
		
		WebElement selectOption = getElementsObj.getElementFromListByContainText(selectOptions, selectType);
			
		selectOptionOne =selectOption.findElements(By.cssSelector("span.pl-2"));
				
//		System.out.println("SIZE:::::: "+ selectOptionOne.size());
			
			WebElement element= getElementsObj.getElement(currentBalanceselectorType, currentBalanceselector);
			 String curentblns = element.getText();
//			 
//			 String debtAmount = selectOptionOne.get(0).getText();
//			 String[] debtAmt = debtAmount.split(":");
//			 System.out.println(debtAmt.length);
//			 System.out.println(debtAmt[0]);
//			 System.out.println(debtAmt[1]);
			String setle = selectOptionOne.get(1).getText();
			 System.out.println(setle);
			 
			 
			if(selectType.equalsIgnoreCase("Select Option #1")) {

			if(selectOptionOne.get(0).getText().contains(curentblns)) {
				System.out.println("Select Option #1: Debt Validation: Success");
				System.out.println("Expected : " + selectOptionOne.get(0).getText() + "Actual : " + curentblns);
			}
			}
			 
		else if(selectType.equalsIgnoreCase("Select Option #2")) {
				
			 if(selectOptionOne.get(0).getText().contains(curentblns)) {
					
					System.out.println("Select Option #2: Debt Validation: Success");
					System.out.println("Expected : " + selectOptionOne.get(0).getText() + "Actual : " + curentblns);											
			 }
			 }
		else if(selectType.equalsIgnoreCase("Select Option #3")) {
					
			 if(selectOptionOne.get(0).getText().contains(curentblns)) {
						
						System.out.println("Select Option #3: Debt Validation: Success");
						System.out.println("Expected : " + selectOptionOne.get(0).getText() + "Actual : " + curentblns);														
			 }
			 }
		else if(selectType.equalsIgnoreCase("Select Option #4")) {
						
			 if(selectOptionOne.get(0).getText().contains(curentblns)) {
							
							System.out.println("Select Option #4: Debt Validation: Success");
							System.out.println("Expected : " + selectOptionOne.get(0).getText() + "Actual : " + curentblns);
			 }
			 }
		else {
					System.out.println("Select Option #: Debt Validation: failed");
					System.out.println("Expected : " + selectOptionOne.get(0).getText() + "Actual : " + curentblns);
					}
				 
				
			
				
						 
			 
				
		}			
			
	
	public void optionTwo(String selectType) throws Throwable{
		
		
		List<WebElement> allTable = new ArrayList();
		List<WebElement> CreditorInfo = new ArrayList();
		List<WebElement> SettelmentAmt = new ArrayList();

		
		
		allTable = getElementsObj.getElementList(credirInfoSelectortype, creditorInfoSelector);
		
//		System.out.println(allTable.size());
		
		WebElement elm =getElementsObj.getElementFromListByContainText(allTable, selectType);
		
//	System.out.println(elm.getText());
	
	CreditorInfo= elm.findElements(By.tagName("tr"));
	
//	System.out.println(CreditorInfo.size());
	
		WebElement elm1 = getElementsObj.getElementFromListByContainText(CreditorInfo, "Estimated Settlement Amount:");
		
	SettelmentAmt= elm1.findElements(By.tagName("td"));
		
//	System.out.println(SettelmentAmt.size());	
	System.out.println(SettelmentAmt.get(3).getText());
	String estsetlamnt = SettelmentAmt.get(3).getText();
	String estimatedSettlementAmount[] = estsetlamnt.split(":");
//	System.out.println(estimatedSettlementAmount[0]);	
	System.out.println(estimatedSettlementAmount[1]);
	String estimateSettlementAmountValue = estimatedSettlementAmount[1];
	System.out.println(estimateSettlementAmountValue);
//		WebElement elm2 = getElementsObj.getElementFromListByContainText(SettelmentAmt, "CI Settlement Amount:");
//		
//		System.out.println(elm2.getText());
//		
//		String sttelementAmt = elm2.getText();
//		
//		System.out.println(sttelementAmt);
	
	List<WebElement> selectOptions = new ArrayList();
	List<WebElement> selectOptionOne = new ArrayList();

	selectOptions = getElementsObj.getElementList(optionsSelectorType, optionsSelector);
	
	WebElement selectOption = getElementsObj.getElementFromListByContainText(selectOptions, "Select Option #1");
		
	selectOptionOne =selectOption.findElements(By.cssSelector("span.pl-2"));
	String setle = selectOptionOne.get(1).getText();
	String settlementAmt[] = setle.split(":");
	
	 System.out.println(settlementAmt[0]);
	 System.out.println(settlementAmt[1]); 
	 String settlementAmount = settlementAmt[1];
	 
	 if(estimateSettlementAmountValue.equalsIgnoreCase(settlementAmount)) {
		 
		 System.out.println("Settelement validation success");
	 }
	 else {System.out.println("Settelement validation Faild");}

	}
	
	
	
	
	
	
	public String getTotallsettlementamount() throws Throwable{
		
		List<WebElement> paymentsfield = new ArrayList();
		
		paymentsfield = getElementsObj.getElementList(selectorType, selector);
		
		System.out.println(paymentsfield.size());
		
//		System.out.println(paymentsfield.get(0).getText());
//		System.out.println(paymentsfield.get(1).getText());
//		System.out.println(paymentsfield.get(2).getText());
//		System.out.println(paymentsfield.get(3).getText());
//		System.out.println(paymentsfield.get(4).getText());
//		System.out.println(paymentsfield.get(5).getText());
//		System.out.println(paymentsfield.get(6).getText());
//		System.out.println(paymentsfield.get(7).getText());
//		System.out.println(paymentsfield.get(8).getText());
//		System.out.println(paymentsfield.get(9).getText());
		System.out.println(paymentsfield.get(10).getText());
//		System.out.println(paymentsfield.get(11).getText());
		
		String totall = paymentsfield.get(10).getText();

		System.out.println("payment fields total amount for Option One " + totall);
	
		return totall;
	}
	
	
	
	public String selectOptionOneElement()throws Throwable {
		

		List<WebElement> options = new ArrayList();
		List<WebElement> allFields = new ArrayList();
		
		options = getElementsObj.getElementList(optionsSelectorType, optionsSelector);
		
		WebElement option = getElementsObj.getElementFromListByContainText(options, "Select Option #1");
		
		allFields = option.findElements(By.cssSelector("span.pl-2"));
//  	System.out.println(allFields.size());
//		String fieldOne = allFields.get(0).getText();
		String settlement = allFields.get(1).getText();
//		String fieldThree = allFields.get(3).getText();
//		String fieldFour = allFields.get(4).getText();
//		System.out.println(settlement);
		
		String settlementAmt[] = settlement.split(": ");
		String settlementAmount = settlementAmt[1];
//		System.out.println(settlementAmount);
		return settlementAmount;
		
	}
	
	public void validateSettlementAmountWithPaymenttotal() throws Throwable{
		
		String actualResult = getTotallsettlementamount();
		
		String expactedResult = selectOptionOneElement();
	
		assertionObj.CompareTwoString(actualResult,expactedResult, "Compare Success");
		
//		Assert.assertEquals(money, honey, "Validation success");
//		System.out.println(money + "&" + honey);
//		
//		if(money.equalsIgnoreCase(honey)) {
//			
//			System.out.println("Validation Success...Settlement Amount is matching with total amount");
//		}
//		else {
//			System.out.println("Validation failed. didn't match total amount on payment section");
//		}
	}
	
	
	
	public void clickOptionTwo() throws Throwable {
		
		waitTimeObj.Thread(60000);
		
		clickObj.click(optionTwoSelectorType, optionTwoSelector);
	}
	
	public String totalPayments() throws Throwable {
		
		waitTimeObj.Thread(2000);
		
		List<WebElement> paymentsfield = new ArrayList();
		paymentsfield = getElementsObj.getElementList(selectorType, selector);
//		System.out.println(paymentsfield.size());
		String totall = paymentsfield.get(10).getText();
		
		System.out.println("payment fields total amount for select option Two " + totall);
		return totall;
	}
	
	public String selectOptionTwo() throws Throwable{
		
		waitTimeObj.Thread(2000);
		
		List<WebElement> options = new ArrayList();
		List<WebElement> allFields = new ArrayList();
		
		options = getElementsObj.getElementList(optionsSelectorType, optionsSelector);
		
		WebElement option = getElementsObj.getElementFromListByContainText(options, "Select Option #2");
		
		allFields = option.findElements(By.cssSelector("span.pl-2"));
		
//		System.out.println(allFields.size());
		
//		String optionTwosettlAmt1 = allFields.get(0).getText();
		String optionTwosettlAmtTField = allFields.get(1).getText();
//		String optionTwosettlAmt3 = allFields.get(2).getText();
//		String optionTwosettlAmt4 = allFields.get(3).getText();
//		String optionTwosettlAmt5 = allFields.get(4).getText();
//		System.out.println( optionTwosettlAmtTField );
			
				String  optionTwoSetlementAmount[] = optionTwosettlAmtTField.split(": ");
				String setlementAmount = optionTwoSetlementAmount[1];
//				System.out.println( setlementAmount );
				return setlementAmount;
	}
	
	public void validateOptionTwoSettlementAmount() throws Throwable {
		
		String actualResult = totalPayments();
		String expectedResult = selectOptionTwo();
		
		assertionObj.CompareTwoString(actualResult,expectedResult, "Validation of Option Two Settlement Amount is Success");
		
	}

public void clickOptionThree() throws Throwable{
	
	waitTimeObj.Thread(2000);
	
	clickObj.click(optionThreeSelectorType, optionThreeSelector);
	
}	
	
	public String getTotalPaymentAmount () throws Throwable {
		
		waitTimeObj.Thread(2000);
		
		List<WebElement> paymentsfield = new ArrayList();
		
		paymentsfield = getElementsObj.getElementList(selectorType, selector);
		
//		System.out.println(paymentsfield.size());
		
		
		String totall = paymentsfield.get(10).getText();
		
		System.out.println("payment fields total amount for select option Three " + totall);
		
		return totall;
	}
	public String getSetlementAmount () throws Throwable{
		
		List<WebElement> options = new ArrayList();
		List<WebElement> allFields = new ArrayList();
		
		options = getElementsObj.getElementList(optionsSelectorType, optionsSelector);
		
		WebElement option = getElementsObj.getElementFromListByContainText(options, "Select Option #3");
		
		allFields = option.findElements(By.cssSelector("span.pl-2"));
		
//		System.out.println(allFields.size());
		
		String settlement = allFields.get(1).getText();
		
		String settlementAmt[] = settlement.split(": ");
		
		String settlementAmount = settlementAmt[1];
		
//		System.out.println(settlementAmount);
		
		return settlementAmount;
	}
	
	public void validateoptionThreeSettlementAmount() throws Throwable{
		String actualResult = getSetlementAmount();
		String expectedResult = getTotalPaymentAmount();
		
		assertionObj.CompareTwoString(actualResult,expectedResult, "Validation of Option Three Settlement Amount is Success");
		
	}
	public void clickOptionFour() throws Throwable{
		
		waitTimeObj.Thread(2000);
		
		clickObj.click(optionFourSelectorType, optionFourSelector);
		
	}
	
	public String getSetlemntAmountForOptionFour() throws Throwable{
		
		List<WebElement> options = new ArrayList();
		List<WebElement> allFields = new ArrayList();
		
		options = getElementsObj.getElementList(optionsSelectorType, optionsSelector);
		
		WebElement option = getElementsObj.getElementFromListByContainText(options, "Select Option #4");
		
		allFields = option.findElements(By.cssSelector("span.pl-2"));
		
//		System.out.println(allFields.size());
		
		String settlement = allFields.get(1).getText();
		
		String settlementAmt[] = settlement.split(": ");
		
		String settlementAmount = settlementAmt[1];
		
//		System.out.println("Total Settlement " + settlementAmount);
		
		return settlementAmount;
	}
	
	public String getTotalAmount_OptionFour () throws Throwable{
		
		waitTimeObj.Thread(2000);
		
		List<WebElement> paymentsfield = new ArrayList();
		
		paymentsfield = getElementsObj.getElementList(selectorType, selector);
		
//		System.out.println(paymentsfield.size());
		
		
		String totall = paymentsfield.get(6).getText();
		
		System.out.println("payment fields total amount for select option Four " + totall);
		
		return totall;
		
	}
	
	public void validateOptionFourSettlementAmount() throws Throwable{
		
		String actualResult = getTotalAmount_OptionFour();
		String expectedResult = getSetlemntAmountForOptionFour();
		
		assertionObj.CompareTwoString(actualResult,expectedResult, "Validation of Option Four Settlement Amount is Success");
		
//		String getsetleamount =paymentsSettlementAmount();
		
//		assertionObj.CompareTwoString(getsetleamount,expectedResult, "Validation of Payment's Settlement Amount is Success");
	}
	
	public void clickAddNewPaymentButton() throws Throwable{
		
		waitTimeObj.Thread(3000);
		
		clickObj.click(addNewPaymentSelectorType, addNewPaymentSelector);
		
		
	}

	public void addPaymentAmount() throws Throwable{
		
		waitTimeObj.Thread(3000);
		
		inputObj.clearText(addPaymentSelectorType, addPaymentSelector);
		
		waitTimeObj.Thread(3000);
		
		 inputObj.enterText(addPaymentSelectorType,paymentAmount, addPaymentSelectorNew);
		 
		
	}
	public void addNewPayments() throws Throwable{
		
		waitTimeObj.Thread(3000);
		
		inputObj.clearText(addAnotherPaymentSelectorType, addAnotherPaymentSelector);
		
		waitTimeObj.Thread(3000);
		
		inputObj.enterText(addAnotherPaymentSelectorType, anotherPaymentAmount, addAnotherPaymentSelector);
		
	}
	
	public void modifyExisitingPayment() throws Throwable{
		
		waitTimeObj.Thread(3000);
		
//		inputObj.clearText(modifyPaymentSelectorType, modifyPymentSelector);
		
		waitTimeObj.Thread(3000);
		
		inputObj.enterText(modifyPaymentSelectorType, modifyPayment, modifyPymentSelector);
		
	}
	
	public void nevigateToSalesforce() throws Throwable{
		
//		waitTimeObj.Thread(8000);
		
//		navigationObj.navigateTo(ClearOneCalculatorProperties.negoCalcUrl);
		
		
		waitTimeObj.Thread(8000);
		
		navigationObj.navigateTo(SalesforceProperties.salesforceUrl);
		
		
		
	}
	
	public void loginToSalesforce() throws Throwable{
		
		clickObj.click(usernameSelectorType, usernameSelector);
		
		inputObj.enterText(addNewPaymentSelectorType, sfUsername, usernameSelector);
		
		waitTimeObj.Thread(5000);
		
		clickObj.click(passSelectorType, passSelector);
		
		inputObj.enterText(passSelectorType, sfPassword, passSelector);
		
	}
	
	public void clickLoginToSandbox() throws Throwable {
		
		waitTimeObj.Thread(5000);
		
		clickObj.click(sandboxSelectorType, sandboxSelector);
		
	}
	
	public void clickNevigationMenu() throws Throwable{
		
		clickObj.click(nevigationMenuSelectorType, nevigationMenuSelector);
		
		
	}
	
	public void clickLeads() throws Throwable{
		
		List<WebElement> menuOptions = new ArrayList();
		
		menuOptions = getElementsObj.getElementList(menuOptionSelectorType, menuOptionSelector);
		 
		 WebElement	 optionLead = getElementsObj.getElementFromListByText(menuOptions, "Leads");
		 
		 waitTimeObj.Thread(5000);
		 
		 optionLead.click();
	}
	
	public void clickNewToCreatNewLead() throws Throwable{
		
		 waitTimeObj.Thread(5000);
		 
		clickObj.click(newButtonSelectorType, newButtonSelector);
		
		
	}
	
	public void enterNewLeadInformation()throws Throwable {
		
		waitTimeObj.Thread(5000);
		
		inputObj.enterText(firstNameSelectorType, firstNameText, firstNameSelector);
		
		waitTimeObj.Thread(5000);
		
		inputObj.enterText(lastNameSelectorType, lastNameText, lastNameSelector);
		
		waitTimeObj.Thread(5000);
		
		inputObj.enterText(ssnSelectorType, ssnText, ssnSelector);
		
		waitTimeObj.Thread(5000);
		
		inputObj.enterText(dobSelectorType, dobText, dobSelector);
		
		waitTimeObj.Thread(5000);
		
		inputObj.enterText(emailSelectorType, emailText, emailSelector);
		
		waitTimeObj.Thread(5000);
		
	}
	public void clickSave () throws Throwable{
		
		clickObj.click(saveButtonSelectorType, saveButtonSelector);
		
		
	}
	
	public void clickOpportunities()throws Throwable{ 
		
		 waitTimeObj.Thread(5000);
		 
		List<WebElement> menuOptions = new ArrayList();
		
		menuOptions = getElementsObj.getElementList(menuOptionSelectorType, menuOptionSelector);
		 
		 WebElement	 optionLead = getElementsObj.getElementFromListByText(menuOptions, "Opportunities");
		 
		 waitTimeObj.Thread(5000);
		 
		 optionLead.click();
		
	}
	public void clickNewToCreateNewOpportunities ()throws Throwable {
		
		 waitTimeObj.Thread(5000);
		 
		clickObj.click(newButtonSelectorType, newButtonSelector);
		
	}
	public void clickNext ()throws Throwable {
		
		waitTimeObj.Thread(5000);
		 
		clickObj.click(nextSelectorType, nextSelector);
	}
	
	public void draftFrequency () throws Throwable{
		
		waitTimeObj.Thread(5000);
		
		List<WebElement>opptyDropDown = getElementsObj.getElementList(draftfrequencyListSelectorType, draftfrequencyListSelector);
		
		System.out.println("Size "+opptyDropDown.size());
		
		waitTimeObj.Thread(3000);
		
		for(WebElement el : opptyDropDown) {
			
			if(el.getText().contains("Draft Frequency")) {
				
				System.out.println("Text of the Element " + el.getText());
				el.findElement(By.xpath(draftfrequencySelector)).click();
						}
			waitTimeObj.Thread(2000);
				break;
		}
	}
	
	
	public void selectDraftFrequency() throws Throwable {
		
		waitTimeObj.Thread(5000);
		
		getElementsObj.getElement(draftFrequencyMonthlySelectorType, draftFrequencyMonthlySelector).click();
	}
	
	public void enterProgramStartDate() throws Throwable {
		
		
		waitTimeObj.Thread(5000);
		
		getElementsObj.getElement(programStartDateSelectorType, programStartDateSelector).click();
		
		inputObj.enterText(programStartDateSelectorType, programStartDateText, programStartDateSelector);
		
	}
	
	public void enterFirstSplitDraftAmount () throws Throwable {
		
		waitTimeObj.Thread(5000);

		inputObj.enterText(firstSplitDraftAmountSelectorType, firstSplitDraftAmount, firstSplitDraftAmountSelector);	
		
		
	}
	public void enterSecondSplitDraftAmount () throws Throwable {
		
		waitTimeObj.Thread(5000);

		inputObj.enterText(secondSplitDraftAmountSelectorType, secondSplitDraftAmount, secondSplitDraftAmountSelector);
		
	}
	
	public void enterDraftNote ()throws Throwable {

		waitTimeObj.Thread(3000);
		
		List<WebElement>draftNoteList = getElementsObj.getElementList(draftNoteListSelectorType, draftNoteListSelector);
		
		for(WebElement el : draftNoteList) {
			
			String elem = el.getText();
			System.out.println(elem);
			
//			if(el.getText().contains("Draft Note")) {
				el.sendKeys("Test Opportunity , dummy data entered");
				
	//			el.click();
//				System.out.println("Text of the Elements: " + el.getText());
				
	//			el.findElement(By.xpath("//div[@class,'slds-form-element__control slds-grow textarea-container']")).sendKeys("Test Opportunity , dummy data entered");
	//			el.sendKeys("Test Opportunity , dummy data entered");
				
				
				
				//   el.click();
			//	el.findElement(By.xpath(draftfrequencySelector)).sendKeys("Test Opportunity , dummy data entered");
				
		}		
		
	}
	
	public void enterCloseDate()throws Throwable {
		waitTimeObj.Thread(3000);
		
		List<WebElement>closeDateList = getElementsObj.getElementList(closeDateListSelectorType, closeDateListSelector);
		
		for(WebElement el : closeDateList) {
			String elem = el.getText();
			System.out.println(elem);
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
